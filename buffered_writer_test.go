package buffer

import (
	"context"
	"fmt"
	"sync"
	"sync/atomic"
	"testing"
	"time"
)

func TestWriteBufferInstant(t *testing.T) {
	writer := func(ctx context.Context, tasks []interface{}) error {
		return nil
	}
	buffer := NewWriteBuffer(10, time.Second/10, writer)
	var wg sync.WaitGroup
	results := uint32(0)
	for i := 0; i < 100; i++ {
		wg.Add(1)
		go func(i int) {
			defer wg.Done()
			err := buffer.Write(context.Background(), []interface{}{i})
			if err == nil {
				atomic.AddUint32(&results, 1)
			}
		}(i)
	}
	wg.Wait()
	if results != 100 {
		t.Fatalf("Expected to see 100 results, got %d", results)
	}
	if len(buffer.results) != 0 {
		t.Fatalf("buffer.results is not empty (leak)")
	}
}

func TestWriteBufferWait(t *testing.T) {
	writer := func(ctx context.Context, tasks []interface{}) error {
		return nil
	}
	buffer := NewWriteBuffer(10, time.Second/10, writer)
	var wg sync.WaitGroup
	results := uint32(0)
	for i := 0; i < 12; i++ {
		wg.Add(1)
		go func(i int) {
			defer wg.Done()
			err := buffer.Write(context.Background(), []interface{}{i})
			if err == nil {
				atomic.AddUint32(&results, 1)
			}
		}(i)
	}
	wg.Wait()
	if results != 12 {
		t.Fatalf("Expected to see 12 results, got %d", results)
	}
	if len(buffer.results) != 0 {
		t.Fatalf("buffer.results is not empty (leak)")
	}
}

func TestWriteBufferError(t *testing.T) {
	writer := func(ctx context.Context, tasks []interface{}) error {
		return fmt.Errorf("!!!")
	}
	buffer := NewWriteBuffer(10, time.Second/10, writer)
	var wg sync.WaitGroup
	results := uint32(0)
	for i := 0; i < 100; i++ {
		wg.Add(1)
		go func(i int) {
			defer wg.Done()
			err := buffer.Write(context.Background(), []interface{}{i})
			if err != nil {
				atomic.AddUint32(&results, 1)
			}
		}(i)
	}
	wg.Wait()
	if results != 100 {
		t.Fatalf("Expected to see 100 results, got %d", results)
	}
	if len(buffer.results) != 0 {
		t.Fatalf("buffer.results is not empty (leak)")
	}
}

func TestWriteBufferDifferentResults(t *testing.T) {
	count := uint32(0)
	writer := func(ctx context.Context, tasks []interface{}) error {
		value := atomic.AddUint32(&count, 1)
		if value == 1 {
			return nil
		} else {
			return fmt.Errorf("!!!")
		}
	}
	buffer := NewWriteBuffer(10, time.Second/10, writer)
	var wg sync.WaitGroup
	results := uint32(0)
	for i := 0; i < 20; i++ {
		wg.Add(1)
		go func(i int) {
			defer wg.Done()
			err := buffer.Write(context.Background(), []interface{}{i})
			if err != nil {
				atomic.AddUint32(&results, 1)
			}
		}(i)
	}
	wg.Wait()
	if results != 10 {
		t.Fatalf("Expected to see 10 results, got %d", results)
	}
	if len(buffer.results) != 0 {
		t.Fatalf("buffer.results is not empty (leak)")
	}
}
