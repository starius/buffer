package buffer

import (
	"context"
	"sync"
	"time"
)

type result struct {
	err  error
	used int
}

type Writer = func(ctx context.Context, tasks []interface{}) error

type WriteBuffer struct {
	maxSize int
	maxWait time.Duration
	writer  Writer

	buffer     []interface{}
	generation int
	results    map[int]result

	mu   sync.Mutex
	cond *sync.Cond
}

func NewWriteBuffer(maxSize int, maxWait time.Duration, writer Writer) *WriteBuffer {
	b := &WriteBuffer{
		maxSize: maxSize,
		maxWait: maxWait,
		writer:  writer,
		results: make(map[int]result),
	}
	b.cond = sync.NewCond(&b.mu)
	return b
}

func (b *WriteBuffer) Write(ctx context.Context, tasks []interface{}) error {
	b.mu.Lock()
	b.buffer = append(b.buffer, tasks...)
	buffer := b.buffer
	generation := b.generation
	iAmWorker := false
	if len(buffer) >= b.maxSize {
		b.buffer = nil
		b.generation++
		iAmWorker = true
	}
	b.mu.Unlock()
	if iAmWorker {
		err := b.writer(ctx, buffer)
		b.mu.Lock()
		b.results[generation] = result{err, len(buffer) - len(tasks)}
		b.mu.Unlock()
		b.cond.Broadcast()
		return err
	}
	if len(buffer) == len(tasks) {
		// First task. Run timer and become a worker if needed.
		time.AfterFunc(b.maxWait, func() {
			iAmWorker := false
			b.mu.Lock()
			buffer := b.buffer
			if b.generation == generation {
				b.buffer = nil
				b.generation++
				iAmWorker = true
			}
			b.mu.Unlock()
			if iAmWorker {
				err := b.writer(ctx, buffer)
				b.mu.Lock()
				b.results[generation] = result{err, len(buffer)}
				b.mu.Unlock()
				b.cond.Broadcast()
			}
		})
	}
	// Not worker.
	var r result
	b.mu.Lock()
	for {
		var has bool
		r, has = b.results[generation]
		if has {
			break
		}
		b.cond.Wait()
	}
	r.used -= len(tasks)
	if r.used == 0 {
		delete(b.results, generation)
	} else {
		b.results[generation] = r
	}
	b.mu.Unlock()
	return r.err
}
